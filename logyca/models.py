from django.db import models

# Create your models here.

class Code(models.Model):
    id = models.AutoField(primary_key=True)
    owner = models.ForeignKey('Enterprise', models.CASCADE , related_name='fk_code', db_column='owner')
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=45, blank=True, null=True)
    
class Enterprise(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(db_column='Name', max_length=45)  # Field name made lowercase.
    nit = models.IntegerField(blank=True, null=True)
    gln = models.IntegerField()
    def __str__(self):
         return '%s %s %s %s' % (self.id, self.name, self.nit, self.gln)

