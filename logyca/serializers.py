from rest_framework import serializers 
from .models import (Code,Enterprise)
from drf_writable_nested.serializers import WritableNestedModelSerializer

class CodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Code 
        fields = '__all__'
        #depth = 1

class EnterpriseSerializer(serializers.ModelSerializer):
    fk_code  = CodeSerializer(many=True,read_only=True)
    class Meta:
        model = Enterprise 
        fields = [
            'id',
            'name',
            'nit',
            'gln',
            'fk_code'
        ]



