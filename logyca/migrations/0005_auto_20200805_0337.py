# Generated by Django 3.0.7 on 2020-08-05 03:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logyca', '0004_auto_20200805_0336'),
    ]

    operations = [
        migrations.AlterField(
            model_name='code',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='enterprise',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
