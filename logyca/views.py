from django.shortcuts import render
from rest_framework import viewsets, filters

from .models import (Code,Enterprise)
from .serializers import (CodeSerializer,EnterpriseSerializer)

class EnterpriseViewSet(viewsets.ModelViewSet):
    serializer_class = EnterpriseSerializer  
    queryset = Enterprise.objects.all() 
    filter_backends = [filters.SearchFilter] 
    search_fields = ['=nit'] 
    http_method_names = ['get','post','patch']
    
class CodeViewSet(viewsets.ModelViewSet):
    serializer_class = CodeSerializer  
    queryset = Code.objects.all() 
    filter_backends = [filters.SearchFilter] 
    search_fields = ['owner__id'] 
    http_method_names = ['get','post','patch']


